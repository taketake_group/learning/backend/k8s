resource "aws_security_group" "k3s_rancher"{
  name = "k3s_rancher"
  ingress{
  from_port  = 22
  to_port    = 22
  protocol   = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }
  ingress{
  from_port  = 80
  to_port    = 80
  protocol   = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }
  ingress{
  from_port  = 8080
  to_port    = 8080
  protocol   = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }
  ingress{
  from_port  = 8088
  to_port    = 8088
  protocol   = "tcp" 
  cidr_blocks = ["0.0.0.0/0"]
  }
  ingress{
  from_port  = 449
  to_port    = 449
  protocol   = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }
  egress{
  from_port  = 0
  to_port    = 0
  protocol   = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
  description = "allow tcp 22 and 8080"
}
