provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_instance" "k3s-test" {
  ami                    = "ami-09b68f5653871885f" # ubuntu18.04
  instance_type          = "t3.medium"
  key_name               = "${aws_key_pair.auth.id}"
  vpc_security_group_ids = [
    "${aws_security_group.k3s_rancher.id}"
  ]
  tags                   = {
    Name = "haga"
    Memo = "k3s-test"
  }
}

resource "aws_key_pair" "auth" {
  key_name = "${var.key_name}"
  public_key = "${file(var.public_key_path)}"
}
