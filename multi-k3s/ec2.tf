# 複数台でのk3sを検証

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_instance" "master-k3s" {
  ami                    = "ami-09b68f5653871885f" # ubuntu18.04
  instance_type          = "t3.medium"
  key_name               = "${aws_key_pair.auth.id}"
  vpc_security_group_ids = [
    "${aws_security_group.k3s_rancher_multi.id}"
  ]
  tags                   = {
    Name  = "multi-k3s-master"
    Users = "haga"
  }
}

resource "aws_instance" "node-k3s" {
  count                  = 2
  ami                    = "ami-09b68f5653871885f" # ubuntu18.04
  instance_type          = "t2.micro"
  key_name               = "${aws_key_pair.auth.id}"
  vpc_security_group_ids = [
    "${aws_security_group.k3s_rancher_multi.id}"
  ]
  tags                   = {
    Name  = "multi-k3s-node"
    Users = "haga"
  }
}

resource "aws_key_pair" "auth" {
  key_name = "${var.key_name}"
  public_key = "${file(var.public_key_path)}"
}
